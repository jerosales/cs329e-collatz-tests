#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    #Test Case 1: test from one integer to itself
    def test_read(self):
        s = "5 5\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 5)
        self.assertEqual(j, 5)

    #Test Case 2: test min to max
    def test_read(self):
        s = "1 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 999999)

    #Test Case 3: Large number to smaller number (goes backwards)
    def test_read(self):
        s = "99 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 99)
        self.assertEqual(j, 1)
 
    # ----
    # eval
    # ----

    
    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    #Test Case 1: Evaluate a integer to itself
    def test_eval_5(self):
        v = collatz_eval(5, 5)
        self.assertEqual(v, 6)

    #Test Case 2: Evaluate the min(1) to the max (999999)
    def test_eval_6(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    #Test Case 3: Evaluate a larger integer to a smaller integer
    def test_eval_7(self):
        v = collatz_eval(100, 1)
        self.assertEqual(v, 119)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    #Test Case 1: Print the result of an integer to itself 
    def test_print(self):
        w = StringIO()
        collatz_print(w, 5, 5, 6)
        self.assertEqual(w.getvalue(), "5 5 6\n")

    #Test Case 2: Print the result of the min(1) to the max(999999)
    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 999999, 525)
        self.assertEqual(w.getvalue(), "1 999999 525\n")

    #Test Case 3: Print the result of a larger integer to a smaller integer
    def test_print(self):
        w = StringIO()
        collatz_print(w, 100, 1, 119)
        self.assertEqual(w.getvalue(), "100 1 119\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    #Test case 1:solve the same one over and over
    def test_solve(self):
        r = StringIO("1 100\n1 100\n1 100\n1 100\n1 100\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "1 100 119\n1 100 119\n1 100 119\n1 100 119\n1 100 119\n")

    #Test Case 2:solve steadly increasing intervals
    def test_solve(self):
        r = StringIO("1 100\n101 200\n201 300\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "1 100 119\n101 200 125\n201 300 128\n")

    #Test Case 3:solve random intervals 
    def test_solve(self):
        r = StringIO("5 5\n1 500\n500 1000\n50 250\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "5 5 6\n1 500 144\n500 1000 179\n50 250 128\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" 
	#pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
